
//pengimplementasian konsep encapsulation
import java.util.ArrayList;
import java.util.Scanner;
//class
class User {
    //atribute private hanya dapat diakses oleh instance dari class yang sama 
    private String username;
    private String password;
    //constructor yang menerima 2 parameter
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    //method getter digunakan untuk mengambil nilai dari atribut private 
    //getter username digunakan untuk mengembalikan nilai dari atribut username
    public String getUsername() {
        return username;
    }
     //getter password digunakan untuk mengembalikan nilai dari atribut password
    public String getPassword() {
        return password;
    }
}