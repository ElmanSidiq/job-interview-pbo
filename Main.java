import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<User> users = new ArrayList<>();

        boolean exit = false;
        while (!exit) {
            System.out.println("Menu:");
            System.out.println("1. Login");
            System.out.println("2. Daftar");
            System.out.println("3. Keluar");
            System.out.print("Pilihan: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.print("Username: ");
                    String loginUsername = scanner.nextLine();
                    System.out.print("Password: ");
                    String loginPassword = scanner.nextLine();

                    boolean loginSuccess = false;
                    for (User user : users) {
                        if (user.getUsername().equals(loginUsername) && user.getPassword().equals(loginPassword)) {
                            loginSuccess = true;
                            break;
                        }
                    }

                    if (loginSuccess) {
                        System.out.println("Login berhasil");
                        // pemanggilan method dari class Menu
                        Menu menu = new Menu();
                        menu.Menu();
                    } else {
                        System.out.println("Username atau password salah");
                    }
                    break;

                case 2:
                    System.out.print("Username: ");
                    String registerUsername = scanner.nextLine();
                    System.out.print("Password: ");
                    String registerPassword = scanner.nextLine();

                    User newUser = new User(registerUsername, registerPassword);
                    users.add(newUser);

                    System.out.println("Akun berhasil dibuat");
                    break;

                case 3:
                    exit = true;
                    System.out.println("Terima kasih, program selesai");
                    break;

                default:
                    System.out.println("Pilihan tidak valid");
            }

            System.out.println();
        }

    }
}