
//implementasi polimorpism
import java.util.*;

//superclass
class tiketPesawat {
   // atribut yang bersifat protected,artinya atribut tersebut dapat dikases oleh kelas turunannya.
   protected String nama;
   protected String asal;
   protected String tujuan;
   protected int jumlah_tiket;
   protected int maskapai;
   protected float total;
   protected int bagasi;

   //constructor yang menerima 7 parameter
   tiketPesawat(String nama, String asal, String tujuan, int jumlah_tiket, int maskapai, float total, int bagasi) {
      this.nama = nama;
      this.asal = asal;
      this.tujuan = tujuan;
      this.jumlah_tiket = jumlah_tiket;
      this.maskapai = maskapai;
      this.total = total;
      this.bagasi = bagasi;
   }

   //method getter digunakan untuk mengambil nilai dari atribut protected
   public String getNama() {
      return nama;
   }

   public String getAsal() {
      return asal;
   }

   public String getTujuan() {
      return tujuan;
   }

   public int getJumlah_tiket() {
      return jumlah_tiket;
   }

   public int getMaskapai() {
      return maskapai;
   }

   public float getTotal() {
      return total;
   }

   public int getBagasi() {
      return bagasi;
   }
   //menggunakan modifikasi akses protected agar dapat diakses oleh kelas turunannya
   protected void Tampil() {
      System.out.println("==================================================");
      System.out.println("          INFORMASI TIKET PESANAN ANDA            ");
      System.out.println("==================================================");
      System.out.println("Nama             :" + getNama());
      System.out.println("Asal             :" + getAsal());
      System.out.println("Tujuan           :" + getTujuan());
      System.out.println("Jumlah Tiket     :" + getJumlah_tiket());
      System.out.println("Maskapai         :" + getMaskapai());
      System.out.println("Bagasi Max       :" + getBagasi() + " Kg");
      System.out.println("SubTotal         :RP." + getTotal());

   }
}

// subclass
class tiketPesawatBisnis extends tiketPesawat {
   private int tambahan;

   //constructor yang menerima 8 parameter
   tiketPesawatBisnis(String nama, String asal, String tujuan, int jumlah_tiket, int maskapai, float total, int bagasi,
         int tambahan) {
      super(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi);
      this.tambahan = tambahan;
   }

   //method getter digunakan untuk mengambil nilai dari atribut protected
   public int getTambahan() {
      return tambahan;
   }

   // method Override dari kelas induk
   @Override
   protected void Tampil() {
      super.Tampil();
      System.out.println();
      System.out.println("Selamat Anda Mendapatkan Bagasi Tambahan Seberat  " + getTambahan()
            + " KG\nKarena Telah Memesan Tiket Bisnis");
      System.out.println();
   }

}

public class Tiket {

   public void Tiket() {

      Scanner input = new Scanner(System.in);

      System.out.println("===============================================");
      System.out.println("SELAMAT DATANG DI LOKET PEMESANAN TIKET PESAWAT");
      System.out.println("   Silahkan Lengkapi Data-Data Dibawah Ini     ");
      System.out.println("===============================================");

      System.out.print("Nama         :");
      String nama = input.nextLine();
      System.out.print("Asal         :");
      String asal = input.nextLine();
      System.out.print("Tujuan       :");
      String tujuan = input.nextLine();
      System.out.print("Jumlah Tiket :");
      int jumlah_tiket = input.nextInt();
      System.out.println();
      System.out.print("Jenis Tiket  :\n1.Ekonomi\n2.Bisnis\nPilihan :");
      int pilihan = input.nextInt();

      if (pilihan == 1) {

         System.out.print("Maskapai :\n1.Garuda Indonesia\n2.Lion Air\n3.Air Asia\nPilihan Maskapai :");
         int maskapai = input.nextInt();
         if (maskapai == 1) {
            float harga = 900000;
            float total = harga * jumlah_tiket;
            int bagasi = 20;
            // objek dari class induk
            tiketPesawat tiket = new tiketPesawat(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi);
            tiket.Tampil();
         } else if (maskapai == 2) {
            float harga = 650000;
            float total = harga * jumlah_tiket;
            int bagasi = 20;
            // objek dari class induk 
            tiketPesawat tiket = new tiketPesawat(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi);
            tiket.Tampil();
         } else if (maskapai == 3) {
            float harga = 750000;
            float total = harga * jumlah_tiket;
            int bagasi = 20;
            // objek dari class induk
            tiketPesawat tiket = new tiketPesawat(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi);
            tiket.Tampil();
         }

      } else if (pilihan == 2) {
         System.out.print("Maskapai :\n1.Garuda Indonesia\n2.Lion Air\n3.Air Asia\nPilihan Maskapai :");
         int maskapai = input.nextInt();
         if (maskapai == 1) {
            float harga = 1200000;
            float total = harga * jumlah_tiket;
            int bagasi = 20;
            int tambahan = 10;
            // objek dari kelas turunan
            tiketPesawatBisnis tiket = new tiketPesawatBisnis(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi,
                  tambahan);
            tiket.Tampil();
         } else if (maskapai == 2) {
            float harga = 850000;
            float total = harga * jumlah_tiket;
            int bagasi = 20;
            int tambahan = 10;
            // objek dari kelas turunan
            tiketPesawatBisnis tiket = new tiketPesawatBisnis(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi,
                  tambahan);
            tiket.Tampil();
         } else if (maskapai == 3) {
            float harga = 1000000;
            float total = harga * jumlah_tiket;
            int bagasi = 20;
            int tambahan = 10;
            // objek dari kelas turunan
            tiketPesawatBisnis tiket = new tiketPesawatBisnis(nama, asal, tujuan, jumlah_tiket, maskapai, total, bagasi,
                  tambahan);
            tiket.Tampil();
         }
      }
   }
}
