# Jawaban Nomor 3
# Mampu menjelaskan konsep dasar OOP

=> Encapsulasi adalah sebuah konsep dalam   oop yang memiliki arti (Pembungkus),dimana sebuah data dibungkus agar terlindungi.

=> Inheritance adalah sebuah konsep dalam oop yang memiliki arti (Penurunan),dimana sebuah class induk dapat menurunkan atribut dan method yang dimilikinya ke class anak.

=>Polimorpism adalah sebuah konsep dalam oop yang memiliki arti (Banyak Bentuk),dimana method yang berbeda-beda meskipun namanya sama.
“Bentuk” di sini dapat kita artikan: isinya berbeda, parameternya berbeda, dan tipe datanya berbeda.
Di polimorpism juga terdapat 2 jenis Method yakni method Overriding dan method Overloading.


- Overriding method adalah sebuah metode yang dipakai untuk kelas induk atau superclass dan nantinya akan dipakai untuk mendefinisikan ulang dengan kelas turunan.


- Overloading adalah sebuah metode yang bisa membuat dua atu lebih metode dengan nama yang sama dalam sebuah kelas, namun tipe dan jumlah argumennya harus berbeda satu sama lain.


=>Class Abstrak adalah class yang masih dalam bentuk abstrak. Karena bentuknya masih abstrak, dia tidak bisa dibuat langsung menjadi objek.
Sebuah class agar dapat disebut class abstrak setidaknya memiliki satu atau lebih method abstrak.
Method abstrak adalah method yang tidak memiliki implementasi atau tidak ada bentuk konkritnya.


=> Manfaat Konsep OOP Di industri
Konse OOP memiliki banyak manfaat yang sangat berarti di industri diantaranya:
- dengan konsep oop data data terlindungi karena adanya konsep Encapsulasi yang tujuannya adalah melindungi data.
- konsep oop sangat efektip jika dikerjakan dengan tim, karena tim pengembang perangkat lunak dapat bekerja secara terstruktur, efisien, dan terorganisir. 

