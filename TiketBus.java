
//Pengimplelentasian class Abstack
import java.util.Scanner;
// class abstract
abstract class PesanTiket {
    // atribut yang bersifat protected,artinya atribut tersebut dapat dikases oleh kelas turunannya.
    protected String asal;
    protected String tujuan;
    protected String namaPenumpang;
    protected int jumlahPenumpang;
    protected int hargaTiket;
    //construktor yang menerima 5 parameter
    public PesanTiket(String asal, String tujuan, String namaPenumpang, int jumlahPenumpang, int hargaTiket) {
        this.asal = asal;
        this.tujuan = tujuan;
        this.namaPenumpang = namaPenumpang;
        this.jumlahPenumpang = jumlahPenumpang;
        this.hargaTiket = hargaTiket;
    }
    //method getter digunakan untuk mengambil nilai dari atribut protected
    public String getAsal() {
        return asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public int getJumlahPenumpang() {
        return jumlahPenumpang;
    }

    public double getHargaTiket() {
        return hargaTiket;
    }
    
    //method abstract
    public abstract int hitungTotalHarga();
    //method abstract
    public abstract void Informasi();

}
//class turunan
class PesanTiketBus extends PesanTiket {
    //konstruktor PesanTiketBuspada kelas PesanTiketBus. 
    //Konstruktor ini memiliki parameter yang digunakan untuk 
    //menginisialisasi atribut-atribut pada kelas induk PesanTiket.
    public PesanTiketBus(String asal, String tujuan, String namaPenumpang, int jumlahPenumpang, int hargaTiket) {
        //super digunakan untuk pemanggilan kosntruktor pada class induk
        super(asal, tujuan, namaPenumpang, jumlahPenumpang, hargaTiket);
    }
    //implementasi method abstract dari clas induk
    public int hitungTotalHarga() {
        return jumlahPenumpang * hargaTiket;
    }
     //implementasi method abstract dari clas induk
    public void Informasi() {
        System.out.println("==================================================");
        System.out.println("          INFORMASI TIKET PESANAN ANDA            ");
        System.out.println("==================================================");
        System.out.println("Nama Penumpang  : " + getNamaPenumpang());
        System.out.println("Asal            : " + getAsal());
        System.out.println("Tujuan          : " + getTujuan());
        System.out.println("Jumlah Penumpang: " + getJumlahPenumpang());
        System.out.println("Harga Tiket     : " + getHargaTiket());
        System.out.println("Total Harga     : " + hitungTotalHarga());
    }

}

public class TiketBus {
    public static void main(String[] args) {
        TiketBus();
    }

    public static void TiketBus() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===========================================");
        System.out.println("SELAMAT DATANG DI LOKET PEMESANAN TIKET BUS");
        System.out.println("  Silahkan Lengkapi Data-Data Dibawah Ini  ");
        System.out.println("===========================================");
        System.out.println();
        System.out.println("Daftar Rute Bus :\n1.Sukabumi - Bandung\n2.Bandung - Bekasi\n3.Bandung - Tasik");
        System.out.println();
        System.out.print("Nama             : ");
        String namaPenumpang = scanner.nextLine();
        System.out.print("Asal             : ");
        String asal = scanner.nextLine();
        System.out.print("Tujuan           : ");
        String tujuan = scanner.nextLine();
        System.out.print("Jumlah Tiket     : ");
        int jumlahPenumpang = scanner.nextInt();

        if (asal.equals("Sukabumi") && tujuan.equals("Bandung")) {
            int hargaTiket = 45000;
            // onjek 
            PesanTiketBus pesanTiketBus = new PesanTiketBus(asal, tujuan, namaPenumpang, jumlahPenumpang, hargaTiket);
            System.out.println();
            //Memangil Method informasi yang di class pesan Tiket bus
            pesanTiketBus.Informasi();
        } else if (asal.equals("Bandung") && tujuan.equals("Bekasi")) {
            int hargaTiket = 850000;
            //objek
            PesanTiketBus pesanTiketBus = new PesanTiketBus(asal, tujuan, namaPenumpang, jumlahPenumpang, hargaTiket);
            System.out.println();
            //Memangil Method informasi yang di class pesan Tiket bus
            pesanTiketBus.Informasi();
        } else if (asal.equals("Bandung") && tujuan.equals("Tasik")) {
            int hargaTiket = 80000;
            //objek
            PesanTiketBus pesanTiketBus = new PesanTiketBus(asal, tujuan, namaPenumpang, jumlahPenumpang, hargaTiket);
            System.out.println();
            //Memangil Method informasi yang di class pesan Tiket bus
            pesanTiketBus.Informasi();
        }
        else{
            System.out.println();
            System.out.println("Tiket Bus Tujuan Anda Saat Ini Tidak Ada");
        }

    }
}