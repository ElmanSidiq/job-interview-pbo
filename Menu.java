import java.util.*;

class Menu {

    public void Menu() {

        Scanner scanner = new Scanner(System.in);

        int pilihan;
        do {
            System.out.println("=======================================");
            System.out.println("         Welcome To Pegi Pegi          ");
            System.out.println("=======================================");
            System.out.println("1. Hotel");
            System.out.println("2. Tiket Pesawat");
            System.out.println("3. Tiket Bus");
            System.out.println("0. Keluar");
            System.out.print("Pilihan Anda: ");
            pilihan = scanner.nextInt();

            switch (pilihan) {
                case 1:
                    BookingHotel booking = new BookingHotel();
                    booking.bookHotel();
                    break;

                case 2:
                    Tiket pesawat = new Tiket();
                    pesawat.Tiket();

                    break;
                case 3:
                    TiketBus bus = new TiketBus();
                    bus.TiketBus();
                    break;

                case 0:
                    System.out.println("Terima kasih! Program selesai.");
                    break;

                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih opsi yang tersedia.");
                    break;
            }

            System.out.println();
        } while (pilihan != 0);
    }
}
