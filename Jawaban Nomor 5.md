# Jawaban Nomor 5
# Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)
 
```java
//Pengimplelentasian class Abstack
import java.util.Scanner;
// class abstract
abstract class PesanTiket {
    // atribut yang bersifat protected,artinya atribut tersebut dapat dikases oleh kelas turunannya.
    protected String asal;
    protected String tujuan;
    protected String namaPenumpang;  
    protected int jumlahPenumpang;
    protected int hargaTiket;
    //construktor yang menerima 5 parameter
    public PesanTiket(String asal, String tujuan, String namaPenumpang, int jumlahPenumpang, int hargaTiket) {
        this.asal = asal;
        this.tujuan = tujuan;
        this.namaPenumpang = namaPenumpang;
        this.jumlahPenumpang = jumlahPenumpang;
        this.hargaTiket = hargaTiket;
    }
    //method getter digunakan untuk mengambil nilai dari atribut protected
    public String getAsal() {
        return asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public int getJumlahPenumpang() {
        return jumlahPenumpang;
    }

    public double getHargaTiket() {
        return hargaTiket;
    }
    
    //method abstract
    public abstract int hitungTotalHarga();
    //method abstract
    public abstract void Informasi();

}
//class turunan
class PesanTiketBus extends PesanTiket {
    //konstruktor PesanTiketBuspada kelas PesanTiketBus. 
    //Konstruktor ini memiliki parameter yang digunakan untuk 
    //menginisialisasi atribut-atribut pada kelas induk PesanTiket.
    public PesanTiketBus(String asal, String tujuan, String namaPenumpang, int jumlahPenumpang, int hargaTiket) {
        //super digunakan untuk pemanggilan kosntruktor pada class induk
        super(asal, tujuan, namaPenumpang, jumlahPenumpang, hargaTiket);
    }
    //implementasi method abstract dari clas induk
    public int hitungTotalHarga() {
        return jumlahPenumpang * hargaTiket;
    }
     //implementasi method abstract dari clas induk
    public void Informasi() {
        System.out.println("==================================================");
        System.out.println("          INFORMASI TIKET PESANAN ANDA            ");
        System.out.println("==================================================");
        System.out.println("Nama Penumpang  : " + getNamaPenumpang());
        System.out.println("Asal            : " + getAsal());
        System.out.println("Tujuan          : " + getTujuan());
        System.out.println("Jumlah Penumpang: " + getJumlahPenumpang());
        System.out.println("Harga Tiket     : " + getHargaTiket());
        System.out.println("Total Harga     : " + hitungTotalHarga());
    }

}

```

=> program diatas menggunkan konsep class abstract, yang mana calss PesanTiket merupakan calss abstractnya yang memiliki 6 atribut bersifat protected yang dapat diakses oleh class turunannya,Sebuah class agar dapat disebut class abstrak setidaknya memiliki satu atau lebih method abstrak.
Method abstrak adalah method yang tidak memiliki implementasi atau tidak ada bentuk konkritnya.pada class diatas terdapat method abstract yaitu :
```java
public abstract int hitungTotalHarga();
   
    public abstract void Informasi();
```

kemudian method tersebut diimplementasikan di class turunanya menjadi.
```java
public int hitungTotalHarga() {
        return jumlahPenumpang * hargaTiket;
    }
     //implementasi method abstract dari clas induk
    public void Informasi() {
        System.out.println("==================================================");
        System.out.println("          INFORMASI TIKET PESANAN ANDA            ");
        System.out.println("==================================================");
        System.out.println("Nama Penumpang  : " + getNamaPenumpang());
        System.out.println("Asal            : " + getAsal());
        System.out.println("Tujuan          : " + getTujuan());
        System.out.println("Jumlah Penumpang: " + getJumlahPenumpang());
        System.out.println("Harga Tiket     : " + getHargaTiket());
        System.out.println("Total Harga     : " + hitungTotalHarga());
    }

```


