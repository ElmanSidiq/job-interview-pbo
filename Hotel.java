
//Implementasi konsep Inheritance
import java.util.Scanner;
import java.util.Hashtable;
import java.util.Set;

//class utama
class Hotel {
    // atribut yang bersifat protected,artinya atribut tersebut dapat dikases oleh kelas turunannya.
    protected String tujuan;
    protected int checkIn;
    protected int checkOut;
    protected int jumlahTamu;
    protected int jumlahKamar;
    protected String bulanTahun;
    protected String booking;
    protected String nama;

    // method seter dan getter disunakan untuk mengatur dan mengambil nilai dari atribut 

    protected void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    protected String getTujuan() {
        return tujuan;
    }

    protected void setCheckIn(int checkIn) {
        this.checkIn = checkIn;
    }

    protected int getCheckIn() {
        return checkIn;
    }

    protected void setCheckOut(int checkOut) {
        this.checkOut = checkOut;
    }

    protected int getCheckOut() {
        return checkOut;
    }

    protected void setBulanTahun(String bulanTahun) {
        this.bulanTahun = bulanTahun;
    }

    protected String getBulanTahun() {
        return bulanTahun;
    }

    protected void setJumlahTamu(int jumlahTamu) {
        this.jumlahTamu = jumlahTamu;
    }

    protected int getJumlahTamu() {
        return jumlahTamu;
    }

    protected void setJumlahKamar(int jumlahKamar) {
        this.jumlahKamar = jumlahKamar;
    }

    protected int getJumlahKamar() {
        return jumlahKamar;
    }

    protected void setBooking(String booking) {
        this.booking = booking;
    }

    protected String getBooking() {
        return booking;
    }

    protected void setNama(String nama) {
        this.nama = nama;
    }

    protected String getNama() {
        return nama;
    }

    protected static Scanner scan() {
        return new Scanner(System.in);
    }
}

// class turunan dari Hotel
class BookingHotel extends Hotel {
    //atribut hargapermalam merupakan hashtable yang digunakan untuk 
    //menyimpan harga permalam dari setiap hotel yang tersedia
    private Hashtable<String, Integer> hargaPermalam;
    
    //constructor yang digunakan untuk menginisialisai Hashtable hargapermalam
    //dalam konstructor ini,kami menambahkan entri entri ke hastable dengan mengaitkan
    //setiap nama hotel dengan harga permalam yang sesuai

    public BookingHotel() {
        hargaPermalam = new Hashtable<>();
        hargaPermalam.put("Djardine", 500000);
        hargaPermalam.put("AsiaAfrika", 350000);
        hargaPermalam.put("Pakuan", 550000);
        hargaPermalam.put("Zest", 400000);
        hargaPermalam.put("Grand Indonesia", 800000);
        hargaPermalam.put("Mercure", 550000);
    }

    public void bookHotel() {

        

        Scanner input = new Scanner(System.in);

        Hashtable<String, String> hashtable = new Hashtable<String, String>();
        hashtable.put("Bandung",
                "1.Djardine        = Rp.500.000\n2.AsiaAfrika      = Rp.350.000");
        hashtable.put("Bogor",
                "1.Pakuan           = Rp.550.000\n2.Zest         = Rp.400.000");
        hashtable.put("Jakarta",
                "1.Grand Indonesia  = Rp.800.000\n2.Mercure      = Rp.550.000");

        Set<String> keys = hashtable.keySet();
        for (String key : keys) {
            System.out.println(key);
        }

        System.out.print("Kota Tujuan     : ");
        String key = scan().nextLine();
        setTujuan(key);

        System.out.println("=========================================================");
        System.out.println("        BERIKUT ADALAH REKOMENDASI HOTEL DI " + key);
        System.out.println("=========================================================");
        System.out.println();
        System.out.println(hashtable.get(key));
        System.out.println("==========================================================");
        System.out.print("Nama            : ");
        String nama = input.nextLine();
        setNama(nama);

        System.out.print("Nama Hotel      :");
        String booking = input.nextLine();
        setBooking(booking);

        int harga = hargaPermalam.get(booking);

        System.out.print("Check in        : ");
        int checkIn = input.nextInt();
        setCheckIn(checkIn);

        String bulan_tahun = input.nextLine();
        setBulanTahun(bulan_tahun);

        System.out.print("Check out       : ");
        int checkOut = input.nextInt();
        setCheckOut(checkOut);

        String bulan_tahun1 = input.nextLine();
        setBulanTahun(bulan_tahun1);

        System.out.print("Jumlah Tamu     : ");
        int jumlahTamu = input.nextInt();
        setJumlahTamu(jumlahTamu);

        System.out.print("Jumlah kamar    : ");
        int jumlahKamar = input.nextInt();
        setJumlahKamar(jumlahKamar);

        int total = (checkOut - checkIn) * (harga * jumlahKamar * jumlahTamu);
        System.out.println();

        System.out.println("=========================================================");
        System.out.println("                      Ringkasan Pesanan                  ");
        System.out.println("=========================================================");
        System.out.println("  Nama             : " + getNama());
        System.out.println("  Hotel            : " + getBooking());
        System.out.println("  Tanggal Check In : " + getCheckIn() + getBulanTahun());
        System.out.println("  Tanggal Check Out: " + getCheckOut() + getBulanTahun());
        System.out.println("  Jumlah Tamu      : " + getJumlahTamu());
        System.out.println("  Jumlah Kamar     : " + getJumlahKamar());
        System.out.println("  Subtotal Booking : " + total);
    }
}