# Jawaban Nomor 2
# Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

1.Class Menu

- Mengimpor pustaka yang diperlukan:
Pada langkah ini, pustaka yang diperlukan, yaitu java.util.*, diimpor agar dapat digunakan
dalam  kode program.
- Membuat kelas Menu:
Kelas Menu dibuat untuk mengatur tampilan menu pada aplikasi Pegi Pegi.
- Membuat method Menu():
Method Menu() dibuat untuk menampilkan menu dan menerima input pilihan dari pengguna. Tujuan
dari   method ini adalah untuk dipanggil oleh kelas Main jika proses login berhasil.
- Membuat looping do-while:
Sebuah looping do-while dibuat untuk menjalankan menu aplikasi Pegi Pegi. Looping ini akan
terus berjalan sampai pengguna memilih untuk keluar.
- Menampilkan menu aplikasi Pegi Pegi:
Di dalam looping do-while, program akan menampilkan menu aplikasi Pegi Pegi yang berisi
opsi Booking Hotel, Tiket Pesawat, dan Tiket Bus kepada pengguna.
- Menggunakan switch case untuk menangani pilihan menu:
Sebuah pernyataan switch case ditambahkan di dalam looping do-while untuk menangani pilihan
menu yang dimasukkan oleh pengguna.
- Ketika pilihan menu adalah Booking Hotel:
Jika pengguna memilih menu Booking Hotel, program akan memanggil method Hotel yang telah
dibuat di kelas Hotel. Tujuan dari method ini adalah untuk melakukan proses booking hotel.
- Ketika pilihan menu adalah Tiket Pesawat:
Jika pengguna memilih menu Tiket Pesawat, program akan memanggil method TiketPesawat yang
telah   dibuat di kelas TiketPesawat. Tujuan dari method ini adalah untuk melakukan proses
pemesanan tiket pesawat.
- Ketika pilihan menu adalah Tiket Bus:
Jika pengguna memilih menu Tiket Bus, program akan memanggil method TiketBus yang telah dibuat
di kelas TiketBus. Tujuan dari method ini adalah untuk melakukan proses pemesanan tiket bus.

2.Konsep Encapsulation:

- Mengimpor pustaka yang diperlukan:
Pada langkah ini, pustaka yang diperlukan, yaitu java.util.ArrayList dan java.util.*,
diimpor agar dapat digunakan dalam kode program.
- Membuat kelas User:
Kelas User memiliki beberapa atribut seperti username dan password. Atribut-atribut ini
akan digunakan untuk menyimpan informasi pengguna.
- Menambahkan konstruktor untuk kelas User:
Konstruktor kelas User dibuat dengan menerima dua parameter, yaitu username dan
password. Konstruktor ini akan digunakan untuk menginisialisasi nilai atribut username dan
password pada saat objek User dibuat.
- Membuat metode getter:
Metode getter dibuat untuk mengembalikan nilai dari atribut username dan password. Metode ini
akan memungkinkan akses ke informasi pengguna dari luar kelas User.
- Membuat kelas Main:
Kelas Main merupakan kelas utama yang akan menjadi titik masuk program.
-Membuat ArrayList untuk menyimpan informasi username dan password:
Sebuah ArrayList dibuat untuk menyimpan informasi username dan password yang diinputkan
oleh pengguna.
- Membuat loop while:
Program akan menjalankan loop while yang akan terus berjalan selama kondisi tertentu terpenuhi.
- Menampilkan menu kepada pengguna:
Dalam loop while, program akan menampilkan menu yang berisi opsi Login, Daftar, dan Keluar
kepada pengguna.
- Menggunakan switch case untuk menangani pilihan menu:
Sebuah pernyataan switch case ditambahkan di dalam loop while untuk menangani pilihan menu
yang dimasukkan oleh pengguna.
- Ketika pilihan menu adalah Login:
Jika pengguna memilih menu Login, maka program akan memanggil method yang ada dalam kelas
Menu. Tujuan dari method ini adalah untuk melakukan proses login.
- Ketika pilihan menu adalah Daftar:
Jika pengguna memilih menu Daftar, pengguna akan diminta untuk memasukkan username dan
password. Informasi ini kemudian akan disimpan dalam ArrayList yang telah dibuat sebelumnya.
- Ketika pilihan menu adalah Keluar:
Jika pengguna memilih menu Keluar, program akan berhenti dan keluar dari loop while.
Melakukan iterasi ulang (loop) langkah-langkah 8-12 selama program masih berjalan:
Setelah proses selesai, program akan kembali ke langkah 8 dan menampilkan menu kepada
pengguna lagi.

3.Konsep Inheritance

- Mengimpor pustaka yang diperlukan:
Pada langkah ini, pustaka yang diperlukan, yaitu java.util.Scanner, java.util.Hashtable, dan
java.util.Set, diimpor agar dapat digunakan dalam kode program.
- Membuat kelas Hotel:
Kelas Hotel dibuat dengan beberapa atribut seperti tujuan, checkIn, checkOut,
jumlahTamu,jumlahKamar, bulanTahun, booking, dan nama. Atribut-atribut ini akan digunakan
untuk menyimpan informasi terkait pemesanan hotel.
- Menambahkan metode setter dan getter:
Metode setter dan getter ditambahkan untuk mengatur dan mengambil nilai dari atribut-atribut
dalam kelas Hotel. Metode-metode ini memungkinkan pengaturan dan pengambilan informasi
yang diperlukan terkait pemesanan hotel.
- Membuat kelas BookingHotel sebagai turunan dari kelas Hotel:
Kelas BookingHotel dibuat sebagai turunan dari kelas Hotel. Dengan melakukan pewarisan,
kelas BookingHotel akan mewarisi atribut dan metode dari kelas Hotel.
- Membuat atribut hargapermalam menggunakan Hashtable:
Atribut hargapermalam dibuat sebagai Hashtable yang digunakan untuk menyimpan harga permalam
dari  setiap hotel yang tersedia. Setiap nama hotel akan dihubungkan dengan harga permalam
yang sesuai dalam Hashtable.
- Membuat konstruktor untuk kelas BookingHotel:
Konstruktor kelas BookingHotel dibuat untuk menginisialisasi Hashtable hargapermalam. Di
dalam konstruktor ini, entri-entri ditambahkan ke Hashtable dengan mengaitkan setiap nama
hotel dengan harga permalam yang sesuai.
- Membuat method bookHotel():
Method bookHotel() dibuat dalam kelas BookingHotel agar nantinya dapat dipanggil di kelas Menu.
Di dalam method ini, terdapat Hashtable yang berisi informasi tentang kota, nama-nama hotel
yang terdaftar, dan harga permalamnya. Method ini akan digunakan untuk melakukan proses
pemesanan hotel.

4.Konsep Polymorphism

=>Mengimpor pustaka yang diperlukan:

- Menggunakan pernyataan import java.util.*; untuk mengimpor seluruh pustaka yang ada di
paket java.util.

=>Membuat class tiketPesawat:

- Membuat class dengan beberapa atribut seperti nama, asal, tujuan, jumlah_tiket, maskapai,
total, dan bagasi. Atribut-atribut ini digunakan untuk menyimpan informasi terkait pemesanan
tiket pesawat.
- Membuat konstruktor untuk kelas tiketPesawat yang menerima beberapa parameter, yaitu nama,
asal, tujuan, jumlah_tiket, maskapai, total, dan bagasi. Konstruktor ini digunakan
untuk menginisialisasi nilai atribut pada saat objek tiket dibuat.
- Membuat method getter untuk mengambil nilai dari atribut-atribut yang bersifat protected.
- Membuat method Tampil() dengan akses protected untuk menampilkan informasi tiket pesawat.

=>Membuat subclass tiketPesawatBisnis:

- Membuat subclass tiketPesawatBisnis yang merupakan turunan dari tiketPesawat.
- Menambahkan atribut tambahan dan constructor untuk menginisialisasi atribut tersebut.
- Override method Tampil() pada kelas tiketPesawatBisnis untuk menambahkan informasi
tambahan mengenai bagasi.

=> Membuat class Tiket:

- Membuat class Tiket untuk melakukan input data tiket pesawat dan menampilkan informasi tiket.
- Membuat method Tiket() di dalam class Tiket, method ini nantinya akan dipanggil di class menu.
- Di dalam method Tiket(), membuat objek Scanner untuk membaca input dari pengguna.
- Menggunakan input.nextLine() dan input.nextInt() untuk membaca input pengguna.
- Membuat percabangan berdasarkan jenis tiket yang dipilih.
- Jika pilihan tiket adalah 1 (ekonomi), meminta pengguna memilih maskapai dan sesuaikan
harga, total, dan bagasi berdasarkan maskapai yang dipilih. Membuat objek dari kelas
tiketPesawat dengan data yang diinputkan dan memanggil method Tampil() pada objek
tersebut.
- Jika pilihan tiket adalah 2 (bisnis), meminta pengguna memilih maskapai dan sesuaikan
harga, total, bagasi, dan tambahan berdasarkan maskapai yang dipilih. Membuat objek dari
kelas tiketPesawatBisnis dengan data yang diinputkan dan memanggil method Tampil() pada
objek tersebut.

5.Class Abstract
=>Mengimport pustaka yang diperlukan:

- Mengimport java.util.Scanner untuk membaca input dari pengguna.

=>Membuat class PesanTiket (kelas abstrak):

- Membuat class abstrak dengan atribut-atribut yang bersifat protected (dapat diakses oleh
kelas turunannya).
- Atribut-atribut yang ada adalah asal, tujuan, namaPenumpang, jumlahPenumpang, dan hargaTiket.
- Membuat konstruktor yang menerima 5 parameter untuk menginisialisasi atribut-atribut pada
kelas ini.
- Membuat method getter untuk mengambil nilai dari atribut-atribut yang bersifat protected.
- Membuat method abstract hitungTotalHarga() yang akan diimplementasikan oleh kelas turunannya.
- Membuat method abstract Informasi() yang akan diimplementasikan oleh kelas turunannya.
- Membuat class PesanTiketBus (kelas turunan):

=> Membuat class PesanTiketBus yang merupakan turunan dari PesanTiket.

- Membuat konstruktor PesanTiketBus dengan parameter yang digunakan untuk
- menginisialisasi atribut-atribut pada kelas induk PesanTiket.
- Mengimplementasikan method hitungTotalHarga() dengan mengalikan jumlahPenumpang dengan hargaTiket.
- Mengimplementasikan method Informasi() untuk menampilkan informasi tiket pesanan.

=>Membuat class TiketBus (kelas utama):

- Mendefinisikan method main() sebagai titik masuk program.
- Mendefinisikan method TiketBus() yang melakukan interaksi dengan pengguna untuk memasukkan
  data tiket bus dan menampilkan informasi tiket.
- Membuat objek Scanner untuk membaca input dari pengguna.
- Menampilkan pesan selamat datang dan meminta pengguna untuk mengisi data tiket.
- Membaca input pengguna menggunakan scanner.nextLine() dan scanner.nextInt().
- Melakukan percabangan berdasarkan nilai asal dan tujuan yang diinputkan.
Jika asal dan tujuan sesuai, membuat objek PesanTiketBus dengan data yang diinputkan dan
memanggil method Informasi() pada objek tersebut.
Jika tidak ada tiket yang sesuai, menampilkan pesan bahwa tiket bus tidak tersedia.
