# Jawaban Nomor 7
# Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

roses bisnis dalam aplikasi Pegi pegi melibatkan beberapa langkah dan interaksi antara pengguna (user) dan sistem. 
Berikut adalah deskripsi proses bisnis pada Hotel,Tiket Pesawat,dan Tiket Bus.

=>Hotel


- Pemesanan Kamar Hotel:

a. Pengguna membuka aplikasi Pegi pegi dan melakukan pencarian hotel berdasarkan lokasi, tanggal check in, tanggal check out, jumlah tamu, jumlah kamar dan nama hotel .

b. Aplikasi Pegi pegi menampilkan daftar hotel yang sesuai dengan kriteria pencarian pengguna, termasuk informasi seperti harga kamar permalam.

c. Aplikasi Pegi pegi menampilkan rincian pesanan hotel.




=>Tiket Pesawat

- Pemesanan Tiket Pesawat:

a. Pengguna membuka aplikasi Pegi pegi dan memilih opsi "Cari Tiket Pesawat".

b. Pengguna memasukkan informasi penerbangan seperti nama, kota asal, kota tujuan, jumlah penumpang, dan kelas penerbangan.

c. Aplikasi Pegi pegi melakukan pencarian dan menampilkan daftar penerbangan yang sesuai dengan kriteria pengguna, termasuk informasi seperti maskapai, harga tiket.

d. Pengguna memilih penerbangan yang diinginkan dari daftar yang ditampilkan.

e. Aplikasi Pegi pegi menampilkan tiket yang dipesan.




=>Tiket Bus


- Pencarian dan Pemilihan Tiket Bus:

a. Pengguna membuka aplikasi Pegi pegi dan memilih opsi "Cari Tiket Bus".

b. Pengguna memasukkan informasi perjalanan seperti kota asal, kota tujuan, nama, jumlah penumpang.

c. Aplikasi Pegi pegi melakukan pencarian dan menampilkan daftar bus yang sesuai dengan kriteria pengguna, termasuk harga tiket.
d. Aplikasi Pegi pegi menampilkan tiket yang dipesan.
 










