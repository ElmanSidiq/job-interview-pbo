# jawaban Nomor 4
# Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

```java
//pengimplementasian konsep encapsulation
import java.util.ArrayList;
import java.util.Scanner;
//class
class User {
    //atribute private hanya dapat diakses oleh instance dari class yang sama 
    private String username;
    private String password;
    //constructor yang menerima 2 parameter
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    //method getter digunakan untuk mengambil nilai dari atribut private 
    //getter username digunakan untuk mengembalikan nilai dari atribut username
    public String getUsername() {
        return username;
    }
     //getter password digunakan untuk mengembalikan nilai dari atribut password
    public String getPassword() {
        return password;
    }
}


```
=>Program Diatas menggunakan konsep Encapsulasi Dimana Class user memiliki 2 atribut yang bersipat private tujuannnya agar data tersebut terlindungi dan mencegah akses langsung dari luar kelas,dan program tersebut juga memuat constructor yang memuat 2 parameter seta memiliki method 2 method geter yang digunakan untuk mengambil nilai dari atribut private.



